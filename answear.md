# метод forEach.

этот метод позволяет перебрать каждый элемент массива, с заданным условием , например 

let arr = [2, 4, 8, 0, -1];
let newArr = [];
arr.forEach((item)=>{newArr.push(item*5)});
console.log(newArr); // [10, 20, 40, 0, -5]

# очистить массив 
можно установить его длину = 0

let arr = [1, 2, 3 , 4, 5]
arr.lenght = 0;

или 
присвоить этому массиву пустой массив
arr = [];

так же можно методом splice
arr.splice(0, arr.lenght);
или еще короче: arr.splice(0);




# как проверить массив или нет
- метод Array.isArray():

let first = [1, 2 , "test"];
let second = "this is string";

console.log(Array.isArray(first)); // true
console.log(Array.isArray(second)); // false


- с помощью instanceof Array:

let first = [1, 2 , "test"];
let second = "this is string";

console.log(first instanceof Array); // true
console.log(second instanceof Array); // false
