"use strict";

function filterBy(array, arrayType) {
  let arrayFilter = array.filter((items) => typeof items !== arrayType);
  return console.log(`Array without ${arrayType}\n` ,  arrayFilter);
}

filterBy (["12", 555, "null" , null, 7, {key1: 123, key2: "000"}, false, [0, "qwerty"], true, "false"], "number");
filterBy (["12", 555, "null" , null, 7, {key1: 123, key2: "000"}, false, [0, "qwerty"], true, "false"], "string");
filterBy (["12", 555, "null" , null, 7, {key1: 123, key2: "000"}, false, [0, "qwerty"], true, "false"],"object");
filterBy (["12", 555, "null" , null, 7, {key1: 123, key2: "000"}, false, [0, "qwerty"], true, "false"],"boolean");
